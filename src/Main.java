//import com.zuitt.example.Car;
import com.zuitt.example.*;

public class Main {
    public static void main(String[] args) {

        System.out.println("Hello world!");

        // OOP
        // stands for "Object-Oriented Programming"
        // OOP is a programming model that allows developers to design software around data or objects, rather than function and logic

        // OOP Concepts
        // Objects - abstract idea that represents something in the real world
        // Class - representation of the object using code
        // Instance - unique copy of the idea, made "physical"

        // Objects
        // States and Attributes - what is the idea about?
        // Behaviours - what can idea do?
        // Example: A person has attributes like name, age, height and weight. And a person can eat, sleep and speak.

        // Four Pillars of OOP
        // 1. Encapsulation
        // Data hiding - the variables of a class will be hidden from other classes and can be accessed only through the methods of the current class
        // Provide a public setter and getter function

        // Create a Car
        Car myCar = new Car();
        myCar.drive();

        // Assign properties of myCar using the setter methods
        myCar.setName("R8");
        myCar.setBrand("Audi");
        myCar.setYearOfMake(2022);

        // to view the properties of myCar using the getter methods
        System.out.println("Car Make and Model: " + myCar.getYearOfMake());
        System.out.println("Car Brand: " + myCar.getBrand());
        System.out.println("Car Name: " + myCar.getName());

        System.out.println("My Car, " + myCar.getYearOfMake() + " " + myCar.getBrand() + " " + myCar.getName() + ".");

        // Composition and Inheritance
            // Both concepts promote code reuse through different approach.
            // "Inheritance" allows modelling an object that is a subset of another object
            // It defines "is a relationship"
            // To design a class on what it is.

        // "Composition" allows modelling an object that is made up of other objects
            // Composed object cannot exist without the other entity
            // It defines "has a relationship"
            // to design a class on what it does
            // Example:
                // A car is a vehicle - inheritance
                // A car has a driver - composition

//        System.out.println("Driver name: " + myCar.getDriverName());
//        myCar.setDriver("John Smith");
//        System.out.println("Driver name: " + myCar.getDriverName());

        // 2. Inheritance
            // can be defined as the process where one class acquires the properties and methods of another class
            // With the use of inheritance, the information is made manageable in hierarchical order.

        Dog myPet = new Dog();
        myPet.setName("Brownie");
        myPet.setColor("White");
        myPet.speak();
        myPet.call();

        System.out.println("Pet name: " + myPet.getName() + ". Breed: " + myPet.getBreed() + ". Color: " + myPet.getColor());


        // 3.


        Person child = new Person();
        child.sleep();
        child.run();
        child.morningGreet();
        child.holidayGreet();


        // 4. Polymorphism
        // Derived from greek word: poly which means many and morph means "forms".
        // In Short "Many Forms".
        // Polymorphism is the ability of an object to take on many forms.
        // This is usually done by function/method overloading/overriding

        // Two main types of Polymorphism
            // Static or Compile time polymorphism
            // Methods with teh same name, but they have different data types and different number of arguments

        StaticPoly myAddition = new StaticPoly();
        // Original method
        System.out.println(myAddition.addition(5, 6));
        // Based on adding arguments
        System.out.println(myAddition.addition(5, 6, 7));
        // based on changing data types
        System.out.println(myAddition.addition(5.5 , 6.6));

        // 2. Dynamic or Runtime Polymorphism
        // Function is overridden by replacing the definition of the method in the parent class to the child class.
        /*
            Parent Class        Child
            name                name
            address             address
                        ->
            work("I am a developer)    work(I'm a manager)
         */

        Child myChild = new Child();
        myChild.speak();

    }


}