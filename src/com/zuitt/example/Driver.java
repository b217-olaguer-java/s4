package com.zuitt.example;

public class Driver {
    // Properties/variables
    private String name;

    // Constructor
    public Driver() {

    }

    public Driver(String name) {
        this.name = name;
    }

    // getter and setter
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

//    public void setAge(int age) {
//        this.age = age;
//    }
}
